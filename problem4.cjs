const inventory = require('./inventory.cjs');
function problem4(inventory) {
    if (arguments.length !== 1||!Array.isArray(inventory) || inventory.length === 0  ) {
        return [];
    }
    const carYear = [];
    for (let index = 0; index < inventory.length; index++) {
        carYear.push(inventory[index].car_year);
    }
    return carYear;
}

module.exports = problem4;