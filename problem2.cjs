const inventory = require('./inventory.cjs');
function problem2(inventory) {
    if (arguments.length !== 1||!Array.isArray(inventory)||inventory.length === 0  ) {
        return [];
    }
    return inventory[inventory.length - 1];
}

module.exports = problem2;