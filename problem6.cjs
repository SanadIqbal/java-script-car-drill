const inventory = require('./inventory.cjs');
function problem6(inventory) {
    if(arguments.length!==1||!Array.isArray(inventory)||inventory.length===0)
    {
    return [];
    }
    const car = [];
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_make === 'BMW' || inventory[index].car_make === 'Audi') {
            car.push(inventory[index]);
        }
    }
    return JSON.stringify(car);
}

module.exports = problem6;