const inventory = require('./inventory.cjs');

function problem1(inventory, carId) {
    if ( arguments.length !== 2|| !Array.isArray(inventory) ||inventory.length === 0 || typeof carId !== 'number') {
        return [];
    }
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].id === carId) {
            return inventory[index];
        }
    }
    return [];
}

module.exports = problem1;