// Problem 3
const inventory = require('./inventory.cjs');
// 
function problem3(inventory) {
    const CarModel = [];
    if(arguments!==1||!Array.isArray(inventory)||inventory.length===0)
    {
    return [];
    }
    for (let index = 0; index < inventory.length; index++) {
        CarModel.push(inventory[index].car_model);
    }
    return CarModel.sort();
}

module.exports = problem3;