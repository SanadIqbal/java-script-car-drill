const inventory = require('./inventory.cjs');
function problem5(result) {
    if(arguments!==1||!Array.isArray(inventory)||result.length===0)
    {
    return 0;
    }
    let count = 0;
    for (let index = 0; index < result.length; index++) {
        if (result[index] < 2000) {
            count++;
        }
    }
    return count;
}

module.exports = problem5;